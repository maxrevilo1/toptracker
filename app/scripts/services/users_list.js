'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.usersList
 * @description
 * # usersList
 * Service in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.service('UsersList', function ($timeout, $firebaseArray, settings, User) {
    const listRef = settings.firebaseRef.child('users/profile');

    class UserList {
        static get() {
            const query = {
                orderBy: '$priority',
                limitToLast: 100,
            };
            return new UserList(query);
        }

        /**
         * @param  {?Object} query
         *         {
         *             {String} orderBy='$priority',
         *             {Any} equalTo,
         *             {Any} startAt,
         *             {Any} endAt,
         *             {Number} limitToFirst,
         *             {Number} limitToLast,
         *         }
         */
        constructor(query) {
            const ref = createRef(this, query);

            return $firebaseArray.call(this, ref);
        }
    }


    _(UserList.prototype).extend({
        $$added: function(snap) {
            const user = User.getOne(snap.key());

            user.$loaded(() => this.$$updated(user));
            user.$watch(() => this.$$updated(user));
            
            return user;
        },

        $$moved: function() {
            return $firebaseArray.prototype.$$moved.apply(this, arguments);
        },

        $$updated: function() {},

        $$removed: function(snap) {
            const user = this.$getRecord(snap.key());
            user.$destroy();
            return $firebaseArray.prototype.$$removed.apply(this, arguments);
        },
    });

    const createRef = function(instance, query = {}) {
        let ref = listRef;

        const {
            orderBy='$priority',
            equalTo=null,
            startAt=null,
            endAt=null,
            limitToFirst=null,
            limitToLast=null,
        } = query;

        switch(orderBy) {
        case '$priority':
            ref = ref.orderByPriority();
            break;
        case '$key':
            ref = ref.orderByKey();
            break;
        case '$value':
            ref = ref.orderByValue();
            break;
        //Then we order by child
        default:
            ref = ref.orderByChild(orderBy);
            break;
        }

        if(equalTo) {
            ref = ref.equalTo(equalTo);
        } else {
            if(startAt) ref = ref.startAt(startAt);
            if(endAt) ref = ref.endAt(endAt);
        }

        if(limitToFirst) ref = ref.limitToFirst(limitToFirst);
        if(limitToLast) ref = ref.limitToLast(limitToLast);

        return ref;
    };

    return $firebaseArray.$extend(UserList);
});
