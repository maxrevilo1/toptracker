'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.modalDecorator
 * @description
 * # modalDecorator
 * Service in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.factory('modalDecorator', function ($mdDialog) {
    return {
        decorate: function(scope) {
            scope.hide = function() {
                $mdDialog.hide();
                return false;
            };

            scope.cancel = function() {
                $mdDialog.cancel();
                return false;
            };

            scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };

            return scope;
        },
    };
});
