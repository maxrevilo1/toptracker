'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.settings
 * @description
 * # settings
 * Service in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.factory('settings', function ($window) {
    const DEV = true;

    if(DEV) {
        return {
            debug: true,
            /******* DEVELOPMENT SETTINGS *******/
            firebaseRef: new $window.Fireproof(new $window.Firebase(
                'https://oliver-toptal-dev.firebaseio.com/v0'
            )),
        };
    } else {
        return {
            debug: false,
            /******* PRODUCTION SETTINGS *******/
            firebaseRef: new $window.Fireproof(new $window.Firebase(
                'https://oliver-toptal.firebaseio.com/v0'
            )),
        };
    }
});
