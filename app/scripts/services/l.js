'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.l
 * @description
 * # l
 * Service in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.factory('l', function () {
    return function(fArg) { return fArg; };
});
