'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.User
 * @description
 * # User
 * Service in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.service('User', function ($window, $q, $firebase, $firebaseObject, settings) {
    const instancesRef = settings.firebaseRef.child('users/profile');
    const expensesIndexRef = settings.firebaseRef.child('users/expenses');
    const rolesRef = settings.firebaseRef.child('users/roles');

    class User {
        static getNewId() {
            return instancesRef.push().key();
        }

        static create(uid, attrs, save=true) {
            const {name, email} = attrs;

            const user = new User(uid);

            const setData = () => {
                user.name = name;
                user.email = email;
            };

            if(save) {
                setData();
                user.$save();
            } else {
                user.$loaded(() => setData());
            }

            return user;
        }

        static getOne(userId) {
            const user = new User(userId);
            return user;
        }

        static addExpenseToUserIndex(expense) {
            return expensesIndexRef
                .child(expense.userId)
                .child(expense.$id)
                .setWithPriority({
                    amount: expense.amount,
                    datetime: expense.datetime,
                }, expense.$priority);
        }

        static removeExpenseToUserIndex(expense) {
            return expensesIndexRef
                .child(expense.userId)
                .child(expense.$id)
                .remove();
        }

        constructor(id) {
            this._isLoaded = false;
            return $firebaseObject.call(this, instancesRef.child(id));
        }
    }

    _(User.prototype).extend({
        isLoaded: function() { return this._isLoaded; },

        getRoles: function() {
            const roles = $firebaseObject(rolesRef.child(this.$id));
            return roles;
        },

        $$updated: function() {
            this._isLoaded = true;
            const changed = $firebaseObject.prototype.$$updated.apply(this, arguments);

            if(changed) {
                setNameInitials(this);
            }

            return changed;
        },

        $save: function() {
            if(!this.createdAt) {
                this.createdAt = $window.Firebase.ServerValue.TIMESTAMP;

                if(!this.$priority) this.$priority = this.createdAt;
            }

            this.updatedAt = $window.Firebase.ServerValue.TIMESTAMP;

            const promise = $firebaseObject.prototype.$save.apply(this, arguments);

            return promise;
        },
    });

    /***** PRIVATE ****/
    function setNameInitials(user) {
        const names = user.name.split(' ');
        if(names.length > 1) {
            user.nameInitials = names[0][0] + names[1][0];
        } else {
            user.nameInitials = user.name[0];
            if(user.name.length > 1) {
                user.nameInitials += user.name[1];
            }
        }
    }

    return $firebaseObject.$extend(User);
});
