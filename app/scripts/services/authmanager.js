'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.AuthManager
 * @description
 * # AuthManager
 * Service in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.factory('AuthManager', function (settings, $firebaseAuth) {
    return new class AuthManager {
        constructor() {
            this.authObj = $firebaseAuth(settings.firebaseRef);
        }

        getAuthObj() { return this.authObj; }

        logout() {
            this.authObj.$unauth();
        }
    };
});
