'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.Expense
 * @description
 * # Expense
 * Factory in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.service('Expense', function ($window, $firebase, $firebaseObject, $q, settings, User) {
    const instancesRef = settings.firebaseRef.child('expenses/profile');

    class Expense {
        static getNewId() {
            return instancesRef.push().key();
        }

        static create(id, attrs, save=true) {
            const {
                userId,
                description,
                amount,
                datetime,
                comments,
            } = attrs;

            //const datetime = Number(datetimeRaw);

            const expense = new Expense(id);

            const setData = () => {
                expense.userId = userId;
                expense.description = description;
                expense.amount = amount;
                expense.datetime = Number(datetime);
                expense.comments = comments || {};
            };

            if(save) {
                setData();
                expense.$save();
            } else {
                expense.$loaded(() => setData());
            }

            return expense;
        }

        static getOne(id) {
            const expense = new Expense(id);
            return expense;
        }

        constructor(id) {
            this._isLoaded = false;
            return $firebaseObject.call(this, instancesRef.child(id));
        }
    }

    _(Expense.prototype).extend({
        isLoaded: function() { return this._isLoaded; },

        $$updated: function(ss) {
            this._isLoaded = true;
            const changed = $firebaseObject.prototype.$$updated.apply(this, arguments);

            if(changed) {
                if(!(this.datetime instanceof Date)) {
                    this.datetime = new Date(this.datetime);
                }
            }

            return changed;
        },

        $save: function() {
            if(!this.createdAt) {
                this.createdAt = $window.Firebase.ServerValue.TIMESTAMP;

                if(!this.$priority) this.$priority = this.createdAt;
            }
            this.updatedAt = $window.Firebase.ServerValue.TIMESTAMP;

            this.datetime = Number(this.datetime);

            const userIndexPromise = User.addExpenseToUserIndex(this);
            const promise = $firebaseObject.prototype.$save.apply(this, arguments);

            return $q.all([promise, userIndexPromise]);
        },

        $remove: function() {
            this.datetime = Number(this.datetime);

            const userIndexPromise = User.removeExpenseToUserIndex(this);
            const promise = $firebaseObject.prototype.$remove.apply(this, arguments);

            return $q.all([promise, userIndexPromise]);
        },
    });

    return $firebaseObject.$extend(Expense);
});
