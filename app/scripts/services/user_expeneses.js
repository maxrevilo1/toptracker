'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.userExpeneses
 * @description
 * # userExpeneses
 * Service in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.service('UserExpeneses', function ($timeout, $firebaseArray, settings, Expense) {
    const listRef = settings.firebaseRef.child('users/expenses');

    const SUNDAY = 0;
    const SATURDAY = 6;

    class UserExpeneses {
        static getLowerDateForWeek(weekOffset = 0) {
            const now = new Date();
            const date = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            date.setDate(now.getDate() - now.getDay() + SUNDAY + 7 * weekOffset);
            return date;
        }
        static getUpperDateForWeek(weekOffset = 0) {
            const now = new Date();
            const date = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            date.setDate(now.getDate() - now.getDay() + SATURDAY + 7 * weekOffset);
            return date;
        }

        static create(userId, weekOffset = 0) {
            const query = {
                orderBy: 'datetime',
                startAt: Number(UserExpeneses.getLowerDateForWeek(weekOffset)),
                endAt: Number(UserExpeneses.getUpperDateForWeek(weekOffset)),
                limitToLast: 100,
            };
            return new UserExpeneses(userId, query);
        }

        /**
         * @param  {String} userId The user ID
         * @param  {?Object} query
         *         {
         *             {String} orderBy='$priority',
         *             {Any} equalTo,
         *             {Any} startAt,
         *             {Any} endAt,
         *             {Number} limitToFirst,
         *             {Number} limitToLast,
         *         }
         */
        constructor(userId, query) {
            const ref = createRef(this, userId, query);

            this._totalAmount = 0;

            return $firebaseArray.call(this, ref);
        }
    }

    _(UserExpeneses.prototype).extend({
        totalAmount: function() { return this._totalAmount; },
        dailyAverage: function() {
            const elapsedDays = (new Date()).getDay() + 1;
            return this._totalAmount / elapsedDays;
        },

        $$added: function(snap) {
            const expense = Expense.getOne(snap.key());

            expense.$loaded(() => this.$$updated(expense));
            expense.$watch(() => this.$$updated(expense));
            
            return expense;
        },

        $$moved: function() {
            return $firebaseArray.prototype.$$moved.apply(this, arguments);
        },

        $$updated: function() {
            sumAmouts(this);
        },

        $$removed: function(snap) {
            const expense = this.$getRecord(snap.key());

            expense.$destroy();

            //Gotta wait the call of $$proccess before make furter calculations
            $timeout(() => {
                sumAmouts(this);
            }, 0);

            return $firebaseArray.prototype.$$removed.apply(this, arguments);
        },
    });

    const sumAmouts = function(instance) {
        instance._totalAmount = 0;
        for (const expense of instance.$list) {
            instance._totalAmount += expense.amount;
        }
    };

    const createRef = function(instance, userId, query = {}) {
        let ref = listRef.child(userId);

        const {
            orderBy='$priority',
            equalTo=null,
            startAt=null,
            endAt=null,
            limitToFirst=null,
            limitToLast=null,
        } = query;

        switch(orderBy) {
        case '$priority':
            ref = ref.orderByPriority();
            break;
        case '$key':
            ref = ref.orderByKey();
            break;
        case '$value':
            ref = ref.orderByValue();
            break;
        //Then we order by child
        default:
            ref = ref.orderByChild(orderBy);
            break;
        }

        if(equalTo) {
            ref = ref.equalTo(equalTo);
        } else {
            if(startAt) ref = ref.startAt(startAt);
            if(endAt) ref = ref.endAt(endAt);
        }

        if(limitToFirst) ref = ref.limitToFirst(limitToFirst);
        if(limitToLast) ref = ref.limitToLast(limitToLast);

        return ref;
    };

    return $firebaseArray.$extend(UserExpeneses);
});
