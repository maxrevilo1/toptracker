'use strict';

/**
 * @ngdoc service
 * @name oliverToptalApp.toastHelper
 * @description
 * # toastHelper
 * Service in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.factory('toastHelper', function ($document, $mdToast) {
    return {
        simpleToast: function(
            $scope, contentText, hideDelay=3000, parentSelector='body', action=null
            ) {
            const toastScope = $scope.$new();
            toastScope.content = contentText;
            toastScope.action = action;
            const toast = $mdToast.show({
                templateUrl: 'views/toast.html',
                scope: toastScope,
                parent : $document[0].querySelector(parentSelector),
                hideDelay: hideDelay,
                position: 'top',
            });

            return toast;
        },
    };
});
