'use strict';

/**
 * @ngdoc overview
 * @name oliverToptalApp
 * @description
 * # oliverToptalApp
 *
 * Main module of the application.
 */
angular
.module('oliverToptalApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngMaterial',
    'firebase',
    'fsm',
    'angularMoment',
])
.run(function($window, $q) {
    $window.Fireproof.bless($q);
})
.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/landing.html',
            controller: 'LandingCtrl',
            controllerAs: 'landing',
        })
        .when('/dashboard', {
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardCtrl',
            controllerAs: 'dashboard',
        })
        .otherwise({
            redirectTo: '/',
        });
});
