'use strict';

/**
 * @ngdoc function
 * @name oliverToptalApp.controller:ExpenseDetailsCtrl
 * @description
 * # ExpenseDetailsCtrl
 * Controller of the oliverToptalApp
 */
angular.module('oliverToptalApp')
.controller('ExpenseDetailsCtrl', function (
    l, $scope, $mdDialog, modalDecorator, expenseId, $mdToast, $document, Expense
    ) {
    
    $scope.showDescription = true;
    $scope.loading = true;
    $scope.requesting = false;

    $scope.expenseId = expenseId;
    $scope.expense = Expense.getOne($scope.expenseId);

    $scope.expense.$loaded(() => {
        $scope.loading = false;
    });

    $scope.save = () => {
        $scope.requesting = true;
        $scope.expense
        .$save()
        .then(() => {
            $scope.requesting = false;
        });
    };

    $scope.remove = () => {
        const toastScope = $scope.$new();

        toastScope.content = l('Please confirm');

        toastScope.action = {
            text: l('Confirm'),
            onClick: () => {
                $scope.requesting = true;
                $scope.expense
                .$remove()
                .then(() => {
                    $scope.requesting = false;
                    $scope.hide();
                });
            },
        };

        $mdToast.show({
            templateUrl: 'views/toast.html',
            scope: toastScope,
            parent : $document[0].querySelector('#expense-details'),
            hideDelay: 5000,
            position: 'bottom',
        });
    };

    modalDecorator.decorate($scope);
});
