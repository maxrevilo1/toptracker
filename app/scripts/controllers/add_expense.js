'use strict';

/**
 * @ngdoc function
 * @name oliverToptalApp.controller:AddExpenseCtrl
 * @description
 * # AddExpenseCtrl
 * Controller of the oliverToptalApp
 */
angular.module('oliverToptalApp')
.controller('AddExpenseCtrl', function($scope, modalDecorator, Expense, userId) {
    function run() {
        const now = new Date();
        now.setSeconds(0);
        now.setMilliseconds(0);

        $scope.expense = {
            userId: $scope.userId,
            description: undefined,
            amount: undefined,
            datetime: now,
            comments: undefined,
        };
    }

    $scope.userId = userId;
    $scope.loading = false;
    $scope.showDescription = true;

    $scope.toggleDescription = () => {
        $scope.showDescription = !$scope.showDescription;
    };

    $scope.create = () => {
        if($scope.loading) return;

        const isValid = $scope.$eval('form.$valid');

        if(!isValid) return false;

        const expense = Expense.create(
            Expense.getNewId(),
            $scope.expense,
        );
        expense
        .$loaded()
        .then(() => {
            $scope.hide();
        });

        return true;
    };

    modalDecorator.decorate($scope);

    run();
});
