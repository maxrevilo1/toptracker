'use strict';

/**
 * @ngdoc function
 * @name oliverToptalApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the oliverToptalApp
 */
angular.module('oliverToptalApp')
.controller('LoginCtrl', function(
    l, $scope, $location, $document, modalDecorator, toastHelper, AuthManager, User
) {
    $scope.loading = false;
    $scope.user = {};

    modalDecorator.decorate($scope);

    $scope.signIn = () => {
        if ($scope.loading) return;

        const isValid = $scope.$eval('loginForm.$valid');

        if (!isValid) return false;

        let loginData = {
            email: $scope.user.email,
            password: $scope.user.password,
        };

        const authObj = AuthManager.getAuthObj();

        let user;

        $scope.loading = true;
        authObj
        .$authWithPassword(loginData)
        .then(function(userData) {
            user = User.getOne(userData.uid);
            return user.$loaded();
        })
        .then(function(user) {
            user.$destroy();
            $scope.hide();

            $location.path('/dashboard');
        })
        .catch(function(error) {
            console.error('Error: ', error);

            toastHelper.simpleToast(
                $scope,
                l('Error at login\n') + error,
                3000,
                '#login-view'
            );
        })
        .finally(function() {
            $scope.loading = false;
        });


        return true;
    };
});
