'use strict';

/**
 * @ngdoc function
 * @name oliverToptalApp.controller:SignUpCtrl
 * @description
 * # SignUpCtrl
 * Controller of the oliverToptalApp
 */
angular.module('oliverToptalApp')
.controller('SignUpCtrl', function (
        l, $scope, $location, $document, modalDecorator, toastHelper, AuthManager, User
    ) {
    $scope.loading = false;
    $scope.user = {};
    $scope.passwordsMatch = false;

    modalDecorator.decorate($scope);

    $scope.doesPasswordsMatch = () => {
        return $scope.user.password == $scope.user.password2;
    };

    $scope.signUp = () => {
        if($scope.loading) return;

        const isValid = $scope.$eval('signUpForm.$valid');

        if(!isValid) return false;

        if(!$scope.doesPasswordsMatch()) {
            toastHelper.simpleToast(
                $scope,
                l('Password does not match!'),
                3000,
                '#login-view'
            );
            return false;
        }

        let loginData = {
            email: $scope.user.email,
            password: $scope.user.password,
        };

        const authObj = AuthManager.getAuthObj();

        let authUserCreated = false;
        let userCreated = false;
        let user;

        $scope.loading = true;
        authObj
        .$createUser(loginData)
        .then(() => {
            authUserCreated = true;
            return authObj.$authWithPassword(loginData);
        })
        .then(function(userData) {
            user = User.create(
                userData.uid,
                $scope.user,
            );
            return user.$loaded();
        })
        .then(function(user) {
            userCreated = true;

            user.$destroy();
            $scope.hide();

            $location.path('/dashboard');
        })
        .catch(function(error) {
            console.error('Error: ', error);

            if(authUserCreated) {
                if(userCreated) {
                    user.$remove();
                    user.$destroy();
                }
                authObj.$removeUser(loginData);
            }

            toastHelper.simpleToast(
                $scope,
                l('Error at signup\n') + error,
                3000,
                '#login-view'
            );
        })
        .finally(function() {
            $scope.loading = false;
        });


        return true;
    };
});
