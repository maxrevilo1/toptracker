'use strict';

/**
 * @ngdoc function
 * @name oliverToptalApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the oliverToptalApp
 */
angular.module('oliverToptalApp')
.controller('DashboardCtrl', function (
    $scope, $location, $mdDialog, $mdSidenav,
    AuthManager, User, UserExpeneses, UsersList,
    ) {

    function runWithUser(currentUserId) {
        $scope.loading = true;
        if($scope.currentUser) $scope.currentUser.$destroy();

        $scope.currentUser = User.getOne(currentUserId);

        $scope.currentUser
        .$loaded(() => {
            $scope.loading = false;
            $scope.setWeek(0);
        });
    }

    $scope.user;
    $scope.roles;
    $scope.currentUser;
    $scope.logout = () => AuthManager.logout();
    $scope.loading = true;
    
    $scope.currentWeek = 0;
    $scope.expenses;

    $scope.openAddExpense = function(ev) {
        $mdDialog.show({
            controller: 'AddExpenseCtrl',
            templateUrl: 'views/add_expense.html',
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
                userId: $scope.currentUser.$id,
            },
        });
    };

    $scope.openExpenseDetails = function(ev, expense) {
        $mdDialog.show({
            controller: 'ExpenseDetailsCtrl',
            templateUrl: 'views/expense_details.html',
            targetEvent: ev,
            clickOutsideToClose: true,
            locals: {
                expenseId: expense.$id,
            },
        });
    };

    $scope.setWeek = (weekOffset) => {
        if($scope.expenses) $scope.expenses.$destroy();

        $scope.loading = true;
        $scope.currentWeek = weekOffset;
        $scope.expenses = UserExpeneses.create(
            $scope.currentUser.$id,
            weekOffset,
        );

        $scope.expenses.$loaded(() => $scope.loading = false);
    };

    $scope.nextWeek = () => {
        $scope.setWeek($scope.currentWeek + 1);
    };

    $scope.prevWeek = () => {
        $scope.setWeek($scope.currentWeek - 1);
    };

    $scope.weekBeginning = () => {
        return UserExpeneses.getLowerDateForWeek($scope.currentWeek);
    };

    $scope.weekEnd = () => {
        return UserExpeneses.getUpperDateForWeek($scope.currentWeek);
    };


    //Checking auth status
    AuthManager.getAuthObj().$onAuth(function(authData) {

        if(authData) {
            const userId = authData.uid;
            $scope.user = User.getOne(userId);
            $scope.roles = $scope.user.getRoles();

            $scope.user.$loaded(() => runWithUser($scope.user.$id));
        } else {
            $location.path('/');
        }

    });

    $scope.$on('$destroy', () => {
        if($scope.user) $scope.user.$destroy();
    });


    /*** Search users directive ***/
    $scope.usersList;

    $scope.toggleUserList = () => {
        $mdSidenav('userList').toggle();

        if(!$scope.usersList) {
            $scope.usersList = UsersList.get();
        }
    };

    $scope.switchUser = (user) => {
        runWithUser(user.$id);
        $scope.toggleUserList();
    };
});
