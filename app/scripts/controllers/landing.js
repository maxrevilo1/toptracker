'use strict';

/**
 * @ngdoc function
 * @name oliverToptalApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the oliverToptalApp
 */
angular.module('oliverToptalApp')
.controller('LandingCtrl', function ($scope, $location, $mdDialog, AuthManager) {

    AuthManager.getAuthObj().$onAuth(function(authData) {
        if(authData) {
            console.log('Logged in as:', authData.uid);
            $location.path('/dashboard');
        }
    });

    _($scope).extend({
        openSignUpView: (ev) => {

            $mdDialog.show({
                controller: 'SignUpCtrl',
                templateUrl: 'views/signup.html',
                targetEvent: ev,
                clickOutsideToClose: true,
            });
        },

        openLoginView: (ev) => {

            $mdDialog.show({
                controller: 'LoginCtrl',
                templateUrl: 'views/login.html',
                targetEvent: ev,
                clickOutsideToClose: true,
            });
        },


    });
});
