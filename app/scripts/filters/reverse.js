'use strict';

/**
 * @ngdoc filter
 * @name oliverToptalApp.filter:reverse
 * @function
 * @description
 * # reverse
 * Filter in the oliverToptalApp.
 */
angular.module('oliverToptalApp')
.filter('reverse', function() {
    return function(items) {
        return items.slice().reverse();
    };
});
