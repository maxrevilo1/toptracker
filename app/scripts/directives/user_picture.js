'use strict';

/**
 * @ngdoc directive
 * @name oliverToptalApp.directive:userPicture
 * @description
 * # userPicture
 */
angular.module('oliverToptalApp')
.directive('userPicture', function () {
    return {
        replace: true,
        templateUrl: 'views/directives/user_picture.html',
        restrict: 'E',
        scope: {
            user: '=',
        },
        link: function postLink(scope, element, attrs) {
        },
    };
});
