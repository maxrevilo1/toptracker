'use strict';

describe('Directive: userPicture', function () {

    // load the directive's module
    beforeEach(angular.mock.module('oliverToptalApp'));
    beforeEach(angular.mock.module('templates'));

    let element;
    let scope;
    let user;

    const userID = '-TEST-USER';
    const userDef = {
        name: 'Test user',
        email: 'test@test.ts',
    };
    const fakeSnapshot = {
        val: () => userDef,
        getPriority: () => 0,
    };

    beforeEach(inject(function ($q, $rootScope, User) {
        scope = $rootScope.$new();

        User.prototype.$loaded = (fn) => {
            fn();
            return $q.when(true);
        };

        user  = User.create(userID, userDef, false);

        scope.user = user;
    }));

    it('should show the initials of the binded user', inject(function ($compile) {
        scope.user.$$updated(fakeSnapshot);
        scope.$digest();

        element = angular.element('<user-picture user=user></user-picture>');
        element = $compile(element)(scope);
        scope.$digest();

        const firstNameElm = element.find('.first-name');
        const secondNameElm = element.find('.last-name');

        expect(firstNameElm.text().trim()).toEqual('T');
        expect(secondNameElm.text().trim()).toEqual('u');
    }));
});
