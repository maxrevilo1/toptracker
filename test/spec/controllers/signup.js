'use strict';

describe('Controller: SignUpCtrl', function () {

    // load the controller's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    var scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();

        $controller('SignUpCtrl', {
            $scope: scope,
        });
    }));

    describe('When the controller is instantiated', function () {
        let mdDialog;

        beforeEach(inject(($mdDialog) => {
            mdDialog = $mdDialog;
            spyOn($mdDialog, 'hide').and.callThrough();
            spyOn($mdDialog, 'cancel').and.callThrough();
        }));

        it('should have scope decorated with hide', function () {
            expect(scope.hide).toEqual(jasmine.any(Function));

            expect(mdDialog.hide).not.toHaveBeenCalled();
            scope.hide();
            expect(mdDialog.hide).toHaveBeenCalled();
        });

        it('should have scope decorated with cancel', function () {
            expect(scope.cancel).toEqual(jasmine.any(Function));

            expect(mdDialog.cancel).not.toHaveBeenCalled();
            scope.cancel();
            expect(mdDialog.cancel).toHaveBeenCalled();
        });

        it('should have scope decorated with answer', function () {
            expect(scope.answer).toEqual(jasmine.any(Function));

            const ref = {};

            expect(mdDialog.hide).not.toHaveBeenCalled();
            scope.answer(ref);
            expect(mdDialog.hide).toHaveBeenCalledWith(ref);
        });

    });

    describe('Check if passwords match', () => {

        it('Should recognize when the passwords match', function() {
            scope.user.password = '1234';
            scope.user.password2 = '1234';
            scope.$apply();

            expect(scope.doesPasswordsMatch()).toBeTruthy();
        });

        it('Should recognize when the passwords do not match', function() {
            scope.user.password = '1234';
            scope.user.password2 = '1235';
            scope.$apply();

            expect(scope.doesPasswordsMatch()).not.toBeTruthy();
        });

    });

    describe('When user signs up and the form is valid', () => {
        beforeEach(function () {
            scope.signUpForm = {$valid: true};
        });

        it('Should not sign up', function() {
            scope.signUpForm = {$valid: true};
            expect(scope.signUp()).toBe(true);
        });
    });

    describe('When user signs up and the form is invalid', () => {
        beforeEach(function () {
            scope.signUpForm = {$valid: false};
        });

        it('Should not sign up', function() {
            expect(scope.signUp()).toBe(false);
        });
    });

    describe('When user signs up ', () => {
        let mdToast;
        beforeEach(inject(function ($mdToast) {
            mdToast = $mdToast;
            spyOn($mdToast, 'show').and.callThrough();
            scope.signUpForm = {$valid: true};
        }));

        it('Should show a toast if the passwords dont match', function() {
            scope.user.password = '1234';
            scope.user.password2 = '1235';

            expect(mdToast.show).not.toHaveBeenCalled();
            expect(scope.signUp()).toBe(false);
            expect(mdToast.show).toHaveBeenCalled();
        });

        it('Should not show a toast if the passwords match', function() {
            scope.user.password = '1234';
            scope.user.password2 = '1234';

            expect(mdToast.show).not.toHaveBeenCalled();
            expect(scope.signUp()).toBe(true);
            expect(mdToast.show).not.toHaveBeenCalled();
        });
    });

    describe('When user signs up and everithing is correct', () => {
        let location;
        let AuthManager;
        let createUserDeferred;
        let authWithPasswordDeferred;
        let User;
        let newUserInstance;
        let userLoadedDeferred;

        beforeEach(inject(function ($q, $location, _AuthManager_, _User_) {
            scope.signUpForm = {$valid: true};
            scope.user.email = `test#{Date.now()}@test.com`;
            scope.user.password = '1234';
            scope.user.password2 = '1234';

            location = $location;
            spyOn(location, 'path');


            AuthManager = _AuthManager_;
            spyOn(AuthManager.getAuthObj(), '$createUser')
                .and.callFake(function() {
                    createUserDeferred = $q.defer();
                    return createUserDeferred.promise;
                });

            spyOn(AuthManager.getAuthObj(), '$authWithPassword')
                .and.callFake(function() {
                    authWithPasswordDeferred = $q.defer();
                    return authWithPasswordDeferred.promise;
                });

            User = _User_;

            spyOn(User.prototype, '$save');

            spyOn(User.prototype, '$loaded')
                .and.callFake(function() {
                    newUserInstance = this;
                    userLoadedDeferred = $q.defer();
                    return userLoadedDeferred.promise;
                });
        }));

        it('Should attempt to create an user', function() {
            const userUID = '123-1234-12345';

            scope.signUp();

            expect(AuthManager.getAuthObj().$createUser)
                .toHaveBeenCalledWith({
                    email: scope.user.email,
                    password: scope.user.password,
                });

            createUserDeferred.resolve({});
            scope.$digest();

            expect(AuthManager.getAuthObj().$authWithPassword).toHaveBeenCalled();

            authWithPasswordDeferred.resolve({uid: userUID});
            scope.$digest();

            expect(User.prototype.$loaded).toHaveBeenCalled();

            userLoadedDeferred.resolve(newUserInstance);
            scope.$digest();

            expect(location.path).toHaveBeenCalledWith('/dashboard');
        });
    });
    
});
