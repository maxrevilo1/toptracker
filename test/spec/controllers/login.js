'use strict';

describe('Controller: LoginCtrl', function () {

    // load the controller's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    let scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();

        $controller('LoginCtrl', {
            $scope: scope,
        });
    }));

    describe('When the controller is instantiated', function () {
        let mdDialog;

        beforeEach(inject(($mdDialog) => {
            mdDialog = $mdDialog;
            spyOn($mdDialog, 'hide').and.callThrough();
            spyOn($mdDialog, 'cancel').and.callThrough();
        }));

        it('should have scope decorated with hide', function () {
            expect(scope.hide).toEqual(jasmine.any(Function));

            expect(mdDialog.hide).not.toHaveBeenCalled();
            scope.hide();
            expect(mdDialog.hide).toHaveBeenCalled();
        });

        it('should have scope decorated with cancel', function () {
            expect(scope.cancel).toEqual(jasmine.any(Function));

            expect(mdDialog.cancel).not.toHaveBeenCalled();
            scope.cancel();
            expect(mdDialog.cancel).toHaveBeenCalled();
        });

        it('should have scope decorated with answer', function () {
            expect(scope.answer).toEqual(jasmine.any(Function));

            const ref = {};

            expect(mdDialog.hide).not.toHaveBeenCalled();
            scope.answer(ref);
            expect(mdDialog.hide).toHaveBeenCalledWith(ref);
        });
    });

    describe('When user signs in and the form is valid', () => {
        beforeEach(function () {
            scope.loginForm = {$valid: true};
        });

        it('Should not sign up', function() {
            scope.loginForm = {$valid: true};
            expect(scope.signIn()).toBe(true);
        });
    });

    describe('When user signs in and the form is invalid', () => {
        beforeEach(function () {
            scope.loginForm = {$valid: false};
        });

        it('Should not sign up', function() {
            expect(scope.signIn()).toBe(false);
        });
    });

    describe('When user signs in and everithing is correct', () => {
        let location;
        let AuthManager;
        let authWithPasswordDeferred;
        let User;
        let userInstance;
        let userLoadedDeferred;

        beforeEach(inject(function ($q, $location, _AuthManager_, _User_) {
            scope.loginForm = {$valid: true};
            scope.user.email = `test#{Date.now()}@test.com`;
            scope.user.password = '1234';

            location = $location;
            spyOn(location, 'path');

            AuthManager = _AuthManager_;

            spyOn(AuthManager.getAuthObj(), '$authWithPassword')
                .and.callFake(function() {
                    authWithPasswordDeferred = $q.defer();
                    return authWithPasswordDeferred.promise;
                });

            User = _User_;

            spyOn(User, 'getOne').and.callThrough();

            spyOn(User.prototype, '$loaded')
                .and.callFake(function() {
                    userInstance = this;
                    userLoadedDeferred = $q.defer();
                    return userLoadedDeferred.promise;
                });
        }));

        it('Should attempt to create an user', function() {
            const userUID = '123-1234-12345';

            scope.signIn();

            expect(AuthManager.getAuthObj().$authWithPassword).toHaveBeenCalled();

            authWithPasswordDeferred.resolve({uid: userUID});
            scope.$digest();

            expect(User.getOne).toHaveBeenCalledWith(userUID);

            expect(User.prototype.$loaded).toHaveBeenCalled();

            userLoadedDeferred.resolve(userInstance);
            scope.$digest();

            expect(location.path).toHaveBeenCalledWith('/dashboard');
        });
    });
});
