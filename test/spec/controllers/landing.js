'use strict';

describe('Controller: LandingCtrl', () => {

    // load the controller's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    let scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(($controller, $rootScope) => {
        scope = $rootScope.$new();

        $controller('LandingCtrl', {
            $scope: scope,
            // place here mocked dependencies
        });
    }));

    describe('Open the sign Up dialog', () => {
        let mdDialog;

        // Initialize the controller and a mock scope
        beforeEach(inject(($mdDialog) => {
            mdDialog = $mdDialog;

            spyOn($mdDialog, 'show').and.callThrough();

            scope.$apply();
        }));

        it('Not been open at the beginning', function() {
            expect(mdDialog.show).not.toHaveBeenCalled();
        });

        it('Been called if openLoginView is executed', function() {
            const ev = {};
            scope.openSignUpView(ev);

            expect(mdDialog.show).toHaveBeenCalledWith(
                jasmine.objectContaining({
                    controller: 'SignUpCtrl',
                    templateUrl: 'views/signup.html',
                    targetEvent: ev,
                })
            );
        });

    });

    describe('Open the login dialog', () => {
        let mdDialog;

        // Initialize the controller and a mock scope
        beforeEach(inject(($mdDialog) => {
            mdDialog = $mdDialog;

            spyOn($mdDialog, 'show').and.callThrough();

            scope.$apply();
        }));

        it('Not been open at the beginning', function() {
            expect(mdDialog.show).not.toHaveBeenCalled();
        });

        it('Been called if openLoginView is executed', function() {
            const ev = {};
            scope.openLoginView(ev);

            expect(mdDialog.show).toHaveBeenCalledWith(
                jasmine.objectContaining({
                    controller: 'LoginCtrl',
                    templateUrl: 'views/login.html',
                    targetEvent: ev,
                })
            );
        });

    });

});
