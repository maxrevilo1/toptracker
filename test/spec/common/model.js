export const describeAsModel = function(modelName, attrs) {
    const id = '-SOME-ID';

    describe('When instantiated', function() {
        let instance;
        const fakeSnapshot = {
            val: () => attrs.exampleInstance,
            getPriority: () => 0,
        };

        beforeEach(function() {
            instance = new attrs.Model(id);
        });

        it('gets the assigned id', function() {
            expect(instance).toBeDefined();
            expect(instance.$id).toEqual(id);
        });

        it('has isLoaded as false by default', function() {
            expect(instance.isLoaded()).toBe(false);
        });

        describe('When updated', function() {

            beforeEach(function() {
                instance.$$updated(fakeSnapshot);
            });

            it('isLoaded changes to true', function() {
                expect(instance.isLoaded()).toBe(true);
            });

            it('has the snapshot values', function() {
                expect(instance).toEqual(
                    jasmine.objectContaining(fakeSnapshot.val())
                );
            });

            it('has the snapshot values', function() {
                expect(instance).toEqual(
                    jasmine.objectContaining(fakeSnapshot.val())
                );
            });
        });


        describe('When saved', function() {

            beforeEach(function() {
                _(instance).extend(attrs.exampleInstance);
                instance.$save();
            });

            it('the instance must have a createdAt property set', function() {
                expect(instance.createdAt).toBeDefined();
            });

            it('the instance must have an updatedAt property set', function() {
                expect(instance.updatedAt).toBeDefined();
            });
        });
    });

    describe('For class functions', function() {
        it(`Creates an instance with ${modelName}.create`, function() {
            const instance = attrs.Model.create(id, attrs.exampleInstance, true);
            expect(instance).toBeDefined();
            expect(instance instanceof attrs.Model).toBeDefined();
        });

        it(`Retreives one instance with ${modelName}.getOne(instanceId)`, function() {
            const instance = attrs.Model.getOne(id);

            expect(instance).toBeDefined();
            expect(instance instanceof attrs.Model).toBeDefined();
            expect(instance.$id).toEqual(id);
        });

        it(`Generates a new Id when calling ${modelName}.getNewId()`, function() {
            const newId = attrs.Model.getNewId(id);

            expect(newId).toBeDefined();
            expect(newId).toEqual(jasmine.any(String));
        });
    });
};