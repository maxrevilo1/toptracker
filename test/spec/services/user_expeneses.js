'use strict';

describe('Service: UserExpeneses', function () {

    // load the service's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // instantiate service
    let UserExpeneses;

    beforeEach(inject(function (_UserExpeneses_) {
        UserExpeneses = _UserExpeneses_;
    }));

    describe('when created new instance', function () {
        let instance;
        const userId = '-TEST-USER-1';

        beforeEach(() => {
            instance = new UserExpeneses(userId);
        });

        it('should has total expenses in 0', function () {
            expect(instance.totalAmount()).toEqual(0);
        });

        it('should has daily average in 0', function () {
            expect(instance.dailyAverage()).toEqual(0);
        });

        it('should be empty', function () {
            expect(instance.$keyAt(0)).toBeFalsy();
        });
    });

    describe('For static methods', function () {
        it('should return a date on sunday when calling getLowerDateForWeek', function () {
            expect(UserExpeneses.getLowerDateForWeek().getDay()).toEqual(0);
        });
        it('should return a date on saturday when calling getLowerDateForWeek', function () {
            expect(UserExpeneses.getUpperDateForWeek().getDay()).toEqual(6);
        });
    });

    

});
