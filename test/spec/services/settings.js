'use strict';

describe('Service: settings', function () {

    // load the service's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // instantiate service
    var settings;
    beforeEach(inject(function (_settings_) {
        settings = _settings_;
    }));

    it('should be an object', function () {
        expect(settings).toEqual(jasmine.any(Object));
    });

    it('should have the debug property', function () {
        expect(settings.debug).toBeDefined();
    });

});
