'use strict';

describe('Service: UsersList', function () {

    // load the service's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // instantiate service
    let UsersList;

    beforeEach(inject(function (_UsersList_) {
        UsersList = _UsersList_;
    }));

    describe('when created new instance', function () {
        let instance;
        const userId = '-TEST-USER-1';

        beforeEach(() => {
            instance = new UsersList(userId);
        });

        it('should be empty', function () {
            expect(instance.$keyAt(0)).toBeFalsy();
        });
    });

});
