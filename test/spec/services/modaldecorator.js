'use strict';

describe('Service: modalDecorator', function () {

    // load the service's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // instantiate service
    var modalDecorator;
    let scope;
    let mdDialog;

    beforeEach(inject(($rootScope, _modalDecorator_, $mdDialog) => {
        scope = $rootScope.$new();

        modalDecorator = _modalDecorator_;

        mdDialog = $mdDialog;
        spyOn($mdDialog, 'hide').and.callThrough();
        spyOn($mdDialog, 'cancel').and.callThrough();
        //scope.$apply();
    }));

    it('should decorate the scope with hide when decorate is called', function () {
        let result = modalDecorator.decorate(scope);
        expect(scope.hide).toEqual(jasmine.any(Function));
        expect(result.hide).toEqual(jasmine.any(Function));

        expect(mdDialog.hide).not.toHaveBeenCalled();
        const callResult = scope.hide();
        expect(mdDialog.hide).toHaveBeenCalled();

        expect(callResult).toBe(false);
    });

    it('should decorate the scope with cancel when decorate is called', function () {
        const result = modalDecorator.decorate(scope);
        expect(scope.cancel).toEqual(jasmine.any(Function));
        expect(result.cancel).toEqual(jasmine.any(Function));

        expect(mdDialog.cancel).not.toHaveBeenCalled();
        const callResult = scope.cancel();
        expect(mdDialog.cancel).toHaveBeenCalled();
        expect(callResult).toBe(false);
    });

    it('should decorate the scope with answer when decorate is called', function () {
        const result = modalDecorator.decorate(scope);
        expect(scope.answer).toEqual(jasmine.any(Function));
        expect(result.answer).toEqual(jasmine.any(Function));

        const ref = {};

        expect(mdDialog.hide).not.toHaveBeenCalled();
        scope.answer(ref);
        expect(mdDialog.hide).toHaveBeenCalledWith(ref);
    });

});
