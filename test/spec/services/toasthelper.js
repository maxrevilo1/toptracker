'use strict';

describe('Service: toastHelper', function () {

    // load the service's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // instantiate service
    let scope;
    let toastHelper;
    let mdToast;

    beforeEach(inject(function ($rootScope, _toastHelper_, $mdToast) {
        scope = $rootScope.$new();
        toastHelper = _toastHelper_;
        mdToast = $mdToast;
        spyOn($mdToast, 'show').and.callThrough();
    }));

    it('should show a Toad when calling simpleToast with the correct parameters', function () {
        const contentText = 'Testing';
        const action = 'action';
        const hideDelay = 6000;

        const result = toastHelper.simpleToast(scope, contentText, hideDelay, undefined, action);

        expect(mdToast.show)
        .toHaveBeenCalledWith(
            jasmine.objectContaining({
                scope: jasmine.objectContaining({
                    content: contentText,
                    action: action,
                }),
                hideDelay: hideDelay,
            })
        );

        expect(result).toBeDefined();
    });

});
