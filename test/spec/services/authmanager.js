'use strict';

describe('Service: AuthManager', function () {
    // instantiate service
    let AuthManager;
    let firebaseAuth;
    const fakeAuthObject = {
        $unauth: function() {},
    };

    beforeEach(function () {
        // load the service's module
        angular.mock.module('oliverToptalApp');

        angular.mock.module(function($provide) {
            $provide.factory('$firebaseAuth', function () {
                firebaseAuth = jasmine.createSpy()
                                   .and.returnValue(fakeAuthObject);
                return firebaseAuth;
            });
        });

        inject(function(_AuthManager_) {
            AuthManager = _AuthManager_;
        });
    });

    describe('When called getAuthObj()', function () {
        let result;

        beforeEach(function () {
            result = AuthManager.getAuthObj();
        });

        it('should return an angularfire authObject', function () {
            expect(result).toBe(fakeAuthObject);
        });

        it('should have called $firebaseAuth', function () {
            expect(firebaseAuth).toHaveBeenCalled();
        });
    });

    
    describe('When called logout()', function () {
        let authObject;
        beforeEach(function () {
            authObject = AuthManager.getAuthObj();
            spyOn(authObject, '$unauth');
        });

        it('should have called $unauth', function () {
            expect(authObject.$unauth).not.toHaveBeenCalled();
            AuthManager.logout();
            expect(authObject.$unauth).toHaveBeenCalled();
        });
    });
});
