'use strict';

describe('Service: l', function () {

    // load the service's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // instantiate service
    let l;
    beforeEach(inject(function (_l_) {
        l = _l_;
    }));

    it('should return the first argument when called', function () {
        const ref = {};
        expect(l(ref)).toBe(ref);
    });

});
