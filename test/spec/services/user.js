'use strict';
import {describeAsModel} from '../common/model';

describe('Service: User', function() {

    // load the service's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // instantiate service
    let User;
    const describeAsModelAttrs = {};
    const userId = '-TEST-USER-1';

    beforeEach(inject(function(_User_) {
        User = _User_;
        describeAsModelAttrs.Model = User;

        describeAsModelAttrs.exampleInstance = {
            name: 'Test user',
            email: 'test@test.ts',
        };
    }));


    // See '../common/model'
    describeAsModel('User', describeAsModelAttrs);
    
    describe('When updated', function() {
        const fakeSnapshot = {
            val: () => describeAsModelAttrs.exampleInstance,
            getPriority: () => 0,
        };
        let instance;
        const id = '-SOME-ID';

        beforeEach(function() {
            instance = new User(id);
            instance.$$updated(fakeSnapshot);
        });

        it('assigns nameInitials', function() {
            expect(instance.nameInitials).toBeDefined();
            expect(instance.nameInitials).toEqual(jasmine.any(String));
        });
    });

    describe('For its unique class functions', function() {
        it(`Add to an user index an expense when called addExpenseToUserIndex`, function() {
            const expenseId = '-TEST-EXPENSE-1';
            const expense = {
                $id: expenseId,
                $priority: 987654321,
                userId: userId,
                description: 'Test Expense',
                amount: 123321,
                datetime: Date.now() + 500000000,
                comments: 'Some comment',
            };
            const promise = User.addExpenseToUserIndex(expense);

            expect(promise).toBeDefined();
            expect(promise.then).toEqual(jasmine.any(Function));
        });
    });

});
