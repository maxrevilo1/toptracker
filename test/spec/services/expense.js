'use strict';
import {describeAsModel} from '../common/model';

describe('Service: Expense', function() {

    // load the service's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // instantiate service
    let scope;
    let Expense;
    const describeAsModelAttrs = {};

    beforeEach(inject(function(_Expense_, _User_, $rootScope) {
        scope = $rootScope.$new();
        Expense = _Expense_;
        describeAsModelAttrs.Model = Expense;

        describeAsModelAttrs.exampleInstance = {
            userId: '-TEST-USER-1',
            description: 'Test Description.',
            amount: 526,
            datetime: new Date(),
            comments: null,
        };
    }));

    // See '../common/model'
    describeAsModel('Expense', describeAsModelAttrs);

    describe('When saved', function() {
        let firebaseObject;
        let User;
        let instance;
        const id = '-SOME-ID';

        beforeEach(inject(function(_User_, $q, $firebaseObject) {
            firebaseObject = $firebaseObject;

            User = _User_;

            instance = new Expense(id);
            
            _(instance).extend(describeAsModelAttrs.exampleInstance);

            spyOn(User, 'addExpenseToUserIndex')
            .and.callFake(function() { return $q.resolve(); });

            spyOn(firebaseObject.prototype, '$save')
            .and.callFake(function() { return $q.resolve(); });

            instance.$save();
        }));

        it('assigns itself to the user´s expenses index', function() {
            scope.$digest();
            expect(firebaseObject.prototype.$save).toHaveBeenCalled();

            scope.$digest();
            expect(User.addExpenseToUserIndex)
                        .toHaveBeenCalledWith(instance);
        });
    });

});
