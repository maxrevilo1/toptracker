'use strict';

describe('Filter: reverse', function () {

    // load the filter's module
    beforeEach(angular.mock.module('oliverToptalApp'));

    // initialize a new instance of the filter before each test
    var reverse;
    beforeEach(inject(function ($filter) {
        reverse = $filter('reverse');
    }));

    it('should return the input reversed', function () {
        var array = [1, 2, 3, 4, 5];
        expect(reverse(array)).toEqual([5, 4, 3, 2, 1]);
    });

});
