const Dashboard = function() {
    this.base = $('#dashboard-view');

    this.userMiniMenu = this.base.$('md-menu');

    this.logoutBtn = $('[ng-click="logout()"]');

    this.expensesList = this.base.$('.expenses-list');
    this.expensesItems = this.base.all(by.repeater('expense in expenses | reverse'));

    this.insertExpenseBts = this.base.all(by.css('[ng-click="openAddExpense($event)"]')).first();

    this.currentSelectedUser = this.base.$('md-toolbar [ng-click="toggleUserList($event)"]');
    this.userSelectView = $('[md-component-id="userList"]');
    this.userSelectViewCloseBtn = this.userSelectView.$('[ng-click="toggleUserList()"]');

    this.expensesFilter = this.base.$('.expenses-filters');
    this.prevWeekBtn = this.expensesFilter.$('[ng-click="prevWeek()"]');
    this.nextWeekBtn = this.expensesFilter.$('[ng-click="nextWeek()"]');
    this.weekRange = this.expensesFilter.$('.range-limits');
    this.weekTotalExpended = this.expensesFilter.$('.total-expenses');
    this.weekDailyAverageExpended = this.expensesFilter.$('.daily-average');

    
    this.goPrevWeek = () => this.prevWeekBtn.click();
    this.goNextWeek = () => this.nextWeekBtn.click();

    this.openUserSelectView = () => this.currentSelectedUser.click();
    this.closeUserSelectView = () => this.userSelectViewCloseBtn.click();

    this.openInsertExpenseForm = () => this.insertExpenseBts.click();

    this.findExpense = (expenseDef) => {
        return this.expensesItems.filter((item) => {
            const hasDescription = item
                    .element(by.binding('expense.description'))
                    .getText()
                    .then((description) => description === expenseDef.description);

            return hasDescription;
        }).first();
    };
};

module.exports = Dashboard;