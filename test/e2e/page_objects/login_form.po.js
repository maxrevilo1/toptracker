const LoginForm = function() {
    this.base = $('#login form');

    this.userEmail = this.base.element(by.model('user.email'));

    this.userPassword = this.base.element(by.model('user.password'));

    this.loginButton = this.base.$('[ng-click="signIn($event)"]');


    this.setEmail = function(email) {
        this.userEmail.sendKeys(email);
    };

    this.setPassword = function(password) {
        this.userPassword.sendKeys(password);
    };

    this.login = function() {
        this.loginButton.click();
    };
};

module.exports = LoginForm;