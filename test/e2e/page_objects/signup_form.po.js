const SignUpForm = function() {
    this.base = $('#signup form');

    this.userName = this.base.element(by.model('user.name'));

    this.userEmail = this.base.element(by.model('user.email'));

    this.userPassword = this.base.element(by.model('user.password'));

    this.userPassword2 = this.base.element(by.model('user.password2'));

    this.signupButton = this.base.$('[ng-click="signUp($event)"]');


    this.setName = function(name) {
        this.userName.sendKeys(name);
    };

    this.setEmail = function(email) {
        this.userEmail.sendKeys(email);
    };

    this.setPassword = function(password) {
        this.userPassword.sendKeys(password);
    };

    this.setPassword2 = function(password2) {
        this.userPassword2.sendKeys(password2);
    };

    this.signUp = function() {
        this.signupButton.click();
    };
};

module.exports = SignUpForm;