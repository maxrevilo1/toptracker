const LoginForm = function() {
    this.base = $('#add-expense form');

    this.description = this.base.element(by.model('expense.description'));
    this.amount = this.base.element(by.model('expense.amount'));
    this.date = this.base.$('input[type="date"]');
    this.time = this.base.$('input[type="time"]');
    this.comments = this.base.element(by.model('expense.comments'));

    this.cancelBtn = this.base.$('[ng-click="cancel()"]');
    this.createBtn = this.base.$('[ng-click="create()"]');

    this.setDescription = (description) => this.description.sendKeys(description);
    this.setAmount = (amount)           => this.amount.sendKeys(amount);
    this.setDate = (date)               => this.date.sendKeys(date);
    this.setTime = (time)               => this.time.sendKeys(time);
    this.setComments = (comments)       => this.comments.sendKeys(comments);

    this.cancel = () => this.cancelBtn.click();
    this.create = () => this.createBtn.click();
};

module.exports = LoginForm;