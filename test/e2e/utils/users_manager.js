const settings = require('../settings');
const _ = require('underscore');
const Firebase = require('Firebase');
const Fireproof = require('fireproof');
const Q = require('q');
require('colors');

Fireproof.bless(Q);

const mainRef = new Fireproof(new Firebase(settings.firebaseURI));
const usersRef = mainRef.child('users');
const usersProfileRef = usersRef.child('profile');
const usersRolesRef = usersRef.child('roles');

const authAsRoot = () =>
    mainRef.authWithCustomToken(settings.firebaseGodToken);

module.exports = {
    createUser: function(userDef) {
        var uid;

        return authAsRoot()
        .then(() => mainRef.createUser(userDef))
        .then((authData) => {
            const userData = {};
            _(userData).extend(userDef);

            uid = authData.uid;

            userData.createdAt = Firebase.ServerValue.TIMESTAMP;
            userData.updatedAt = Firebase.ServerValue.TIMESTAMP;

            const priority = userData.createdAt;

            return usersProfileRef
                .child(authData.uid)
                .setWithPriority(userData, priority);
        })
        .then(() => mainRef.unauth())
        .then(() => uid);
    },
    
    createAdmin: function(userDef) {
        var uid;

        return this.createUser(userDef)
        .then((rUid) => {
            uid = rUid;
            return authAsRoot();
        })
        .then(() => {
            return usersRolesRef
                .child(uid)
                .child('admin')
                .set(true);
        })
        .then(() => mainRef.unauth())
        .then(() => uid);
    },

    removeUser: function(userCredentials) {
        var uid;
        return mainRef.authWithPassword(userCredentials)
        .then((authData) => {
            uid = authData.uid;
            return authAsRoot();
        })
        .then(() => mainRef.removeUser(userCredentials))
        .then(() => {
            return mainRef
            .child('expenses/profile')
            .orderByChild('userId')
            .equalTo(uid)
            .once('value');
        })
        .then((bookingsSnapshot) => {
            const ps = [];
            ps[0] = usersRef.child(`profile/${uid}`).remove();
            ps[1] = usersRef.child(`expenses/${uid}`).remove();
            ps[2] = usersRef.child(`roles/${uid}`).remove();

            bookingsSnapshot.forEach((bookingSS) => {
                const id = bookingSS.key();
                ps.push(
                    mainRef.child(`expenses/profile/${id}`).remove()
                );
            });

            return Q.all(ps);
        })
        .then(() => mainRef.unauth())
        .then(() => uid);
    },
};
