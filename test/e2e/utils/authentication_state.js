module.exports = {
    forceLogout: function(browser) {
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
        browser.refresh();
    },

    forceLogin: function(browser, firebasePath, credentials) {
        const loginScript = `(
            new window.Firebase("${firebasePath}")
                .authWithPassword(
                    ${JSON.stringify(credentials)},
                    function(e,ad) {}
                )
        );`;
        browser.executeScript(loginScript);
        browser.refresh();
    },
};