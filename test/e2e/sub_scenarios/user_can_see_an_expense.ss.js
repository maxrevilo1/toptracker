const EC = protractor.ExpectedConditions;
//Page Objects
const Dashboard = require('../page_objects/dashboard.po.js');

exports.test = function(expenseDef) {
    describe('User can see an expense:'.yellow, () => {

        it('Should be on the dashboard'.green, () => {
            expect(browser.getLocationAbsUrl()).toMatch('/dashboard');
        });

        const dashboard = new Dashboard();

        it('Should allow the User see the expenses list'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.expensesList), 5000);
            expect(dashboard.expensesList.isPresent()).toBe(true);
        });

        it('Should allow the User see the expected expense'.green, () => {
            expect(dashboard.findExpense(expenseDef).isPresent()).toBe(true);
        });
    });
};