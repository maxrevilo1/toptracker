const EC = protractor.ExpectedConditions;
//Page objects
const SignUpForm = require('../page_objects/signup_form.po.js');

exports.test = function(name, email, password, password2) {
    describe('User SignUp Path:'.yellow, function() {

        it('Should be on the landing page'.green, () => {
            expect(browser.getLocationAbsUrl()).toMatch('/');
        });

        describe('When the user opens the sign up form'.yellow, () => {

            it('Should allow the user to click the sign up button at the toolbar'.green, () => {
                const signupBtn = $('.get-in-section button');

                signupBtn.click();
            });

            const signUpDialog = $('md-dialog#signup');
            it('Should open the sign up form'.green, () => {
                expect(signUpDialog.isPresent()).toBe(true);
            });

            const signUpForm = new SignUpForm();

            it('Should show the sign up fields'.green, () => {
                browser.wait(EC.visibilityOf(signUpForm.base), 1000);

                expect(signUpForm.userName.isPresent()).toBe(true);
                expect(signUpForm.userEmail.isPresent()).toBe(true);
                expect(signUpForm.userPassword.isPresent()).toBe(true);
                expect(signUpForm.userPassword2.isPresent()).toBe(true);
            });

            it('Should allow the sign up'.green, () => {
                signUpForm.setName(name);
                signUpForm.setEmail(email);
                signUpForm.setPassword(password);
                signUpForm.setPassword2(password2);
                signUpForm.signUp();

                return browser.driver.wait(() => {
                    return browser.driver
                        .getCurrentUrl()
                        .then( (url) => /dashboard/.test(url) );
                }, 10000);
            
            });
        });
    });
};
