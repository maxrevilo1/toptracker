const EC = protractor.ExpectedConditions;
//Page Objects
const Dashboard = require('../page_objects/dashboard.po.js');

exports.test = function(expectedExpenses) {
    describe('User can change the week from the dashboard:'.yellow, () => {

        it('Should be on the dashboard'.green, () => {
            expect(browser.getLocationAbsUrl()).toMatch('/dashboard');
        });

        const dashboard = new Dashboard();

        it('Should allow the User to see the current week'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.expensesFilter), 5000);

            expect(dashboard.expensesFilter.isPresent()).toBe(true);
            expect(dashboard.weekRange.isPresent()).toBe(true);

        });

        it('Should allow the User to see the next and previous week buttons'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.expensesFilter), 5000);

            expect(dashboard.prevWeekBtn.isPresent()).toBe(true);
            expect(dashboard.nextWeekBtn.isPresent()).toBe(true);
        });

        it('Should allow go to the next week and see the week empty'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.expensesFilter), 5000);

            dashboard.goNextWeek();

            expect(dashboard.expensesItems.count()).toBe(0);
        });

        it('Should allow the user to go back to the previous week'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.expensesFilter), 5000);

            dashboard.goPrevWeek();

            expect(dashboard.expensesItems.count()).toBe(expectedExpenses);
        });
    });
};