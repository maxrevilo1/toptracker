const EC = protractor.ExpectedConditions;
//Page Objects
const Dashboard = require('../page_objects/dashboard.po.js');
const InsertExpenseForm = require('../page_objects/insert_expense_form.po');

exports.test = function(expenseDef) {
    describe('User cancel before inserting a new expense:'.yellow, () => {

        it('Should be on the dashboard'.green, () => {
            expect(browser.getLocationAbsUrl()).toMatch('/dashboard');
        });

        const dashboard = new Dashboard();

        it('Should show at least one insert expense button'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.insertExpenseBts), 5000);
        });

        const insertExpenseForm = new InsertExpenseForm();

        it('Should open the insert expense form when the button is clicked'.green, () => {
            dashboard.openInsertExpenseForm();
            
            browser.wait(EC.visibilityOf(insertExpenseForm.base), 5000);
        });

        it('Should allow to fill the form'.green, () => {
            expect(insertExpenseForm.description.isPresent()).toBe(true);
            expect(insertExpenseForm.amount.isPresent()).toBe(true);
            expect(insertExpenseForm.date.isPresent()).toBe(true);
            expect(insertExpenseForm.time.isPresent()).toBe(true);
            expect(insertExpenseForm.comments.isPresent()).toBe(true);

            insertExpenseForm.setDescription(expenseDef.description);
            insertExpenseForm.setAmount(expenseDef.amount);
            insertExpenseForm.setComments(expenseDef.comments);
        });

        it('Should close the form´s dialog when the cancel button is pressed'.green, () => {
            insertExpenseForm.cancel();
            expect(insertExpenseForm.base.isPresent()).toBe(false);
        });
    });
};