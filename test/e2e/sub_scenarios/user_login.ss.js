const EC = protractor.ExpectedConditions;
//Page Objects
const LoginForm = require('../page_objects/login_form.po.js');

exports.test = function(email, password) {
    describe('User Logs in:'.yellow, () => {

        it('Should be on the landing page'.green, () => {
            expect(browser.getLocationAbsUrl()).toMatch('/');
        });

        describe('When the user opens the signin form'.yellow, () => {

            it('Should allow the user to click the sign in button at the toolbar'.green, () => {
                const signupBtn = $('.md-toolbar-tools .md-button');

                signupBtn.click();
            });

            const loginDialog = $('md-dialog#login');

            it('Should open the login form'.green, () => {
                expect(loginDialog.isPresent()).toBe(true);
            });

            const loginForm = new LoginForm();

            it('Should show the login fields'.green, () => {
                browser.wait(EC.visibilityOf(loginForm.base), 1000);

                expect(loginForm.userEmail.isPresent()).toBe(true);
                expect(loginForm.userPassword.isPresent()).toBe(true);
                expect(loginForm.loginButton.isPresent()).toBe(true);
            });

            it('Should allow the login with the correct credentials'.green, () => {
                loginForm.setEmail(email);
                loginForm.setPassword(password);

                loginForm.login();

                return browser.driver.wait(() => {
                    return browser.driver
                        .getCurrentUrl()
                        .then( (url) => /dashboard/.test(url) );
                }, 10000);
            
            });
        });
    });
};