const EC = protractor.ExpectedConditions;
//Page Objects
const Dashboard = require('../page_objects/dashboard.po.js');

exports.test = function() {
    describe('User can see week statistics:'.yellow, () => {

        it('Should be on the dashboard'.green, () => {
            expect(browser.getLocationAbsUrl()).toMatch('/dashboard');
        });

        const dashboard = new Dashboard();

        it('Should allow the User to see the current week'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.expensesFilter), 5000);

            expect(dashboard.expensesFilter.isPresent()).toBe(true);
            expect(dashboard.weekRange.isPresent()).toBe(true);

        });

        it('Should allow the User to see the next and previous week buttons'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.prevWeekBtn), 5000);
            browser.wait(EC.visibilityOf(dashboard.nextWeekBtn), 5000);

            expect(dashboard.prevWeekBtn.isPresent()).toBe(true);
            expect(dashboard.nextWeekBtn.isPresent()).toBe(true);
        });

        it('Should allow the User to see the week total and day average expended'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.weekTotalExpended), 5000);
            browser.wait(EC.visibilityOf(dashboard.weekDailyAverageExpended), 5000);

            expect(dashboard.weekTotalExpended.isPresent()).toBe(true);
            expect(dashboard.weekDailyAverageExpended.isPresent()).toBe(true);
        });
    });
};