const EC = protractor.ExpectedConditions;
//Page Objects
const Dashboard = require('../page_objects/dashboard.po.js');

exports.test = function() {
    describe('User Logs out:'.yellow, () => {

        it('Should be on the dashboard'.green, () => {

            return browser.driver.wait(() => {
                return browser.driver
                    .getCurrentUrl()
                    .then( (url) => /dashboard/.test(url) );
            }, 10000);
        });

        const dashboard = new Dashboard();

        it('Should show the logout option when click on user mini menu'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.userMiniMenu), 5000);

            dashboard.userMiniMenu.click();
            
            browser.wait(EC.visibilityOf(dashboard.logoutBtn), 5000);

            expect(dashboard.logoutBtn.isPresent()).toBe(true);
        });

        it('Should log out when the user click on the logout button'.green, () => {
            dashboard.logoutBtn.click();

            expect(browser.getLocationAbsUrl()).toMatch('/');
        });
    });
};


