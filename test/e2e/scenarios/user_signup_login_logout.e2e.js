const settings = require('../settings');
const AuthState = require('../utils/authentication_state.js');
const usersManager = require('../utils/users_manager');
//Page Objects
const userSignUpSubScenario = require('../sub_scenarios/user_signup.ss.js');
const userLoginSubScenario = require('../sub_scenarios/user_login.ss.js');
const userLogoutSubScenario = require('../sub_scenarios/user_logout.ss.js');

describe('User Authentication tests (Sign Up, Login, Logout):'.yellow, () => {

    const name = `Test-User ${Date.now()}`;
    const email = `test-user${Date.now()}@text.ts`;
    const password = '12345T';
    const password2 = '12345T';

    it('Should automatically redirect to the landing because the user is not logged in'.green, () => {
        AuthState.forceLogout(browser);
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');

        expect(browser.getLocationAbsUrl()).toMatch('/');
    });

    userSignUpSubScenario.test(name, email, password, password2);

    userLogoutSubScenario.test();

    it('Should automatically redirect to the landing because the user has logged out'.green, () => {
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');
        browser.refresh();

        expect(browser.getLocationAbsUrl()).toMatch('/');
    });

    userLoginSubScenario.test(email, password);

    userLogoutSubScenario.test();

    it('Should automatically redirect to the landing because the user has logged out a second time'.green, () => {
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');
        browser.refresh();

        expect(browser.getLocationAbsUrl()).toMatch('/');

        browser.wait(() => usersManager.removeUser({email: email, password: password})
            .then((uid) => {
                console.log(`User removed ${uid}`.blue);
                return true;
            })
        );
    });

});