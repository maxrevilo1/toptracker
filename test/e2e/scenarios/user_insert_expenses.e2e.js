const settings = require('../settings');
const AuthState = require('../utils/authentication_state');
const usersManager = require('../utils/users_manager');
//Page Objects
//Sub scenarios
const userSignUpSubScenario = require('../sub_scenarios/user_signup.ss');
const userLoginSubScenario = require('../sub_scenarios/user_login.ss');
const userLogoutSubScenario = require('../sub_scenarios/user_logout.ss');
const userCanSeeWeekStatisticsSubScenario = require('../sub_scenarios/user_can_see_week_statistics.ss');
const userInsertExpenseSubScenario = require('../sub_scenarios/user_insert_expense.ss');
const userCanChangeWeekSubScenario = require('../sub_scenarios/user_can_change_week.ss');
const userCancelBeforeInsertExpenseSubScenario = require('../sub_scenarios/user_cancel_insert_expense.ss');


describe('User inserts expense path:'.yellow, function() {

    const name = `Test-User ${Date.now()}`;
    const email = `test-user${Date.now()}@text.ts`;
    const password = '12345T';
    const password2 = '12345T';

    const expenseDef = {
        description: `Test expense ${Date.now()}-${Math.random().toString(36)}`,
        amount: Math.random() * 200 | 0,
        comments: 'This is a expense generated on a test',
    };

    const expense2Def = {
        description: `Test expense ${Date.now()}-${Math.random().toString(36)}`,
        amount: Math.random() * 200 | 0,
        comments: 'This is a expense generated on a test a second time',
    };

    it('Should automatically redirect to the landing because the user is not logged in'.green, () => {
        AuthState.forceLogout(browser);
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');

        expect(browser.getLocationAbsUrl()).toMatch('/');
    });

    userSignUpSubScenario.test(name, email, password, password2);

    userCanSeeWeekStatisticsSubScenario.test();

    userCanChangeWeekSubScenario.test(0);

    userInsertExpenseSubScenario.test(expenseDef);

    userCanChangeWeekSubScenario.test(1);

    userCancelBeforeInsertExpenseSubScenario.test(expenseDef);

    userLogoutSubScenario.test();

    it('Should automatically redirect to the landing because the user has logged out'.green, () => {
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');
        browser.refresh();

        expect(browser.getLocationAbsUrl()).toMatch('/');
    });

    userLoginSubScenario.test(email, password);

    userCanSeeWeekStatisticsSubScenario.test();

    userCancelBeforeInsertExpenseSubScenario.test(expenseDef);

    userInsertExpenseSubScenario.test(expense2Def);

    userCanChangeWeekSubScenario.test(2);

    userLogoutSubScenario.test();

    it('Should automatically redirect to the landing because the user has logged out a second time'.green, () => {
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');
        browser.refresh();

        expect(browser.getLocationAbsUrl()).toMatch('/');

        browser.wait(() => usersManager.removeUser({email: email, password: password})
            .then((uid) => {
                console.log(`User removed ${uid}`.blue);
                return true;
            })
        );
    });

});