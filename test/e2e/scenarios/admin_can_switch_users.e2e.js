const EC = protractor.ExpectedConditions;
const settings = require('../settings');
const AuthState = require('../utils/authentication_state');
const usersManager = require('../utils/users_manager');
//Page Objects
const Dashboard = require('../page_objects/dashboard.po.js');
//Sub scenarios
const userLoginSubScenario = require('../sub_scenarios/user_login.ss');
const userLogoutSubScenario = require('../sub_scenarios/user_logout.ss');
const userCanSeeWeekStatisticsSubScenario = require('../sub_scenarios/user_can_see_week_statistics.ss');
const userInsertExpenseSubScenario = require('../sub_scenarios/user_insert_expense.ss');
const userCanChangeWeekSubScenario = require('../sub_scenarios/user_can_change_week.ss');
const userCancelBeforeInsertExpenseSubScenario = require('../sub_scenarios/user_cancel_insert_expense.ss');


describe('Admin can switch users path:'.yellow, function() {

    const name = `Admin-User ${Date.now()}`;
    const email = `admin-user${Date.now()}@text.ts`;
    const password = '1234AT';

    const adminDef = {
        name: name,
        email: email,
        password: password,
    };

    const userDef = {
        name: `Test-User ${Date.now()}`,
        email: `test-user${Date.now()}@text.ts`,
        password: '12345T',
    };

    const expenseDef = {
        description: `Test expense ${Date.now()}-${Math.random().toString(36)}`,
        amount: Math.random() * 200 | 0,
        comments: 'This is a expense generated on a test',
    };

    it('Should automatically redirect to the landing because the admin is not logged in'.green, () => {
        browser.wait(() => usersManager.createUser(userDef)
            .then((uid) => {
                console.log(`User created ${uid}`.blue);
                return true;
            })
        );
        browser.wait(() => usersManager.createAdmin(adminDef)
            .then((uid) => {
                console.log(`Admin created ${uid}`.blue);
                return true;
            })
        );

        AuthState.forceLogout(browser);
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');

        expect(browser.getLocationAbsUrl()).toMatch('/');
    });

    userLoginSubScenario.test(email, password);

    userCanSeeWeekStatisticsSubScenario.test();

    userCanChangeWeekSubScenario.test(0);


    describe('Admin can switch users:'.yellow, () => {
        const dashboard = new Dashboard();

        it('Should show to the admin the selected user'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.currentSelectedUser), 5000);
        });

        it('Should open the switch user bar'.green, () => {
            dashboard.openUserSelectView();

            browser.wait(EC.visibilityOf(dashboard.userSelectView), 5000);
        });
        it('Should close the switch user bar'.green, () => {
            browser.wait(EC.visibilityOf(dashboard.userSelectViewCloseBtn), 5000);

            dashboard.closeUserSelectView();

            browser.wait(EC.invisibilityOf(dashboard.userSelectView), 5000);
        });
    });


    userInsertExpenseSubScenario.test(expenseDef);

    userCanChangeWeekSubScenario.test(1);

    userCancelBeforeInsertExpenseSubScenario.test(expenseDef);

    userLogoutSubScenario.test();

    it('Should automatically redirect to the landing because the admin has logged out a second time'.green, () => {
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');
        browser.refresh();

        expect(browser.getLocationAbsUrl()).toMatch('/');

        browser.wait(() => usersManager.removeUser(userDef)
            .then((uid) => {
                console.log(`User removed ${uid}`.blue);
                return true;
            })
        );
        browser.wait(() => usersManager.removeUser(adminDef)
            .then((uid) => {
                console.log(`Admin removed ${uid}`.blue);
                return true;
            })
        );
    });

});