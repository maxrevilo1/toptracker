const settings = require('../settings');
const EC = protractor.ExpectedConditions;
const AuthState = require('../utils/authentication_state.js');
//Page objects
const LoginForm = require('../page_objects/login_form.po.js');

describe('User Login Path with Wrong credentials:'.yellow, function() {

    const WRONG_EMAIL = 'wrong@wrong.com';
    const WRONG_PASSWORD = 'wrong';

    it('Should automatically redirect to the landing because the user is not logged in'.green, () => {
        AuthState.forceLogout(browser);
        browser.driver.get(settings.pageBaseURI + '/#/dashboard');

        expect(browser.getLocationAbsUrl()).toMatch('/');
    });

    describe('When the user opens the signin form'.yellow, () => {

        it('Should allow the user to click the sign in button at the toolbar'.green, () => {
            const signupBtn = $('.md-toolbar-tools .md-button');

            signupBtn.click();
        });


        const loginDialog = $('md-dialog#login');
        it('Should open the login form'.green, () => {
            expect(loginDialog.isPresent()).toBe(true);
        });

        const loginForm = new LoginForm();

        it('Should show the login fields'.green, () => {
            browser.wait(EC.visibilityOf(loginForm.base), 1000);

            expect(loginForm.userEmail.isPresent()).toBe(true);
            expect(loginForm.userPassword.isPresent()).toBe(true);
        });

        it('Should not allow login with the wrong credentials'.green, () => {
            loginForm.setEmail(WRONG_EMAIL);
            loginForm.setPassword(WRONG_PASSWORD);
            loginForm.login();

            /* mdToast is poorly implemented so we can not test this:
            const toast = $('md-toast');
            browser.wait(EC.visibilityOf(toast), 1000);
            expect(toast.isPresent()).toBe(true);
            */
           
            // Istead we expect that the form is still on sight:
            browser.wait(EC.visibilityOf(loginForm.base), 1000);

            expect(loginForm.base.isPresent()).toBe(true);
           
            expect(browser.getLocationAbsUrl()).not.toMatch('/dashboard');
        });
    });
});