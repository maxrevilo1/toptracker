var settings = require('./e2e/settings');

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    
    framework: 'jasmine',

    capabilities: {
        'browserName': 'chrome',
    },

    specs: [
        './e2e/scenarios/*.e2e.js',
    ],

    allScriptsTimeout: 20000,

    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000,
        // defaultTimeoutInterval: 1800000,
        //print: function() {},
    },

    onPrepare: function(){
        require('colors');

        var SpecReporter = require('jasmine-spec-reporter');
        jasmine.getEnv().addReporter(new SpecReporter({
            displayStacktrace: 'summary', //(all|specs|summary|none)
        }));

        browser.driver.get(settings.pageBaseURI);

        // Borwser Logs
        /*afterEach(function () {
            browser
                .manage()
                .logs()
                .get('browser')
                .then(function(browserLogs) {
                    browserLogs.forEach(function(log){
                        if (log.level.value > 900) {
                            console.error(String(log.message).red);
                        } else {
                            console.log(log.message);
                        }
                    });
                });
        });*/
    },
};