import Firebase from 'Firebase';
import Fireproof from 'fireproof';
import Q from 'q';
import 'colors';

Fireproof.bless(Q);

/****** Constants ******/
export const fbURL = 'https://oliver-toptal-dev.firebaseio.com//v0';

/* Private */
const fbTestToken = 'LYYqBt5O49L2mzGPd4j2pp4CTzT6yAqcyk2e2bKp';

const authAsRoot = async function() {
    await mainRef.authWithCustomToken(fbTestToken);
};


const USERS_COUNT = 10;
const EXPENSES_PER_USER_COUNT = 100;

const mainRef = new Fireproof(new Firebase(fbURL));

const usersRef = mainRef.child('users');
const usersProfileRef = usersRef.child('profile');
const usersExpensesRef = usersRef.child('expenses');

const expensesRef = mainRef.child('expenses');
const expensesProfileRef = expensesRef.child('profile');

(async function() {
    console.info(`
        Adding fixtures to the DB
    `);

    //Creating users:
    for(let u = 0; u < USERS_COUNT; u++) {
        try {
            const userId = await createUserWithProfile(u);

            const expensesPromises = [];

            for(let e = 0; e < EXPENSES_PER_USER_COUNT; e++) {
                expensesPromises.push(userAddExpense(userId, e));
            }

            await *expensesPromises;
            console.log(`Created all expenses for user ${u}`.green);
        } catch(e) {
            console.error(`Error with user ${u}`.red, e);
        }
    }


    console.log('Script finished'.blue);
    process.exit();

})();

async function createUserWithProfile(u) {
    const name = `Fixture-User ${u}`;
    const email = `fixture_user${u}@fixture.fx`;
    const password = `12345${u}`;

    const userCred = {
        email: email,
        password: password,
    };

    try {
        const {uid} = await mainRef.createUser(userCred);

        console.info(`Created Authentication ${uid} for user ${name}`);

        await mainRef.authWithPassword(userCred);

        console.info(`Authenticated as ${uid}`);

        const createdAt = {'.sv': 'timestamp'};
        const updatedAt = {'.sv': 'timestamp'};
        const priority = createdAt;

        const userProfile = {
            name: `Fixture-User ${u}`,
            email: `fixture_user${u}@fixture.fx`,
            createdAt: createdAt,
            updatedAt: updatedAt,
        };

        await usersProfileRef
            .child(uid)
            .setWithPriority(userProfile, priority);

        console.info(`New user created ${uid}`);

        return uid;
        
    } catch(error) {
        await authAsRoot();

        await mainRef.removeUser(userCred);
        console.info(`Deleted Authentication for ${userCred.email}`.red);

        await mainRef.unauth();
        console.info('unauthenticated'.red);

        throw error;
    }
}


async function userAddExpense(userId, e) {
    const expenseId = `${userId}:${e}`;

    const createdAt = {'.sv': 'timestamp'};
    const updatedAt = {'.sv': 'timestamp'};
    const priority = createdAt;

    const weekDiff = 1000 * 60 * 60 * 24 * 7;
    const dateDiff = weekDiff * (Math.round(Math.random() * 5) - 4);
    const datetime = Date.now() + dateDiff;

    const expenseDef = {
        userId: userId,
        description: `Fixture Expense #${e}`,
        amount: Math.round(Math.random() * 300),
        datetime: datetime,
        comments: `Pellentesque non sem euismod, rhoncus arcu quis, tincidunt massa`,
        createdAt: createdAt,
        updatedAt: updatedAt,
    };

    try {
        const indexP = usersExpensesRef
            .child(userId)
            .child(expenseId)
            .setWithPriority({
                amount: expenseDef.amount,
                datetime: expenseDef.datetime,
            }, priority);

        const instanceP = expensesProfileRef
            .child(expenseId)
            .setWithPriority(expenseDef, priority);

        await *[indexP, instanceP];

        console.info(`Expense ${e} inserted for user ${userId}`);

        return expenseId;
        
    } catch(error) {
        throw error;
    }
}